﻿using DAL.App.Interfaces;
using DAL.Interfaces;
using DAL.Interfaces.Helpers;
using DAL.Interfaces.Repositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF
{
    public class AppUnitOfWork : IAppUnitOfWork
    {
        private readonly ApplicationDBContext _dbContext;
        private readonly IRepositoryProvider _repositoryProvider;

        public AppUnitOfWork(IDataContext dbContext, IRepositoryProvider repositoryProvider)
        {
            _dbContext = dbContext as ApplicationDBContext;
            if (_dbContext == null)
            {
                throw new ArgumentNullException(nameof(dbContext));
            }

            _repositoryProvider = repositoryProvider;
        }

        public IRepository<Person> People => GetEntityRepository<Person>();

        public IRepository<Experience> Experiences => GetEntityRepository<Experience>();

        public IRepository<Skills> Skills => GetEntityRepository<Skills>();

        public IRepository<Language> Languages => GetEntityRepository<Language>();


        public IRepository<TEntity> GetEntityRepository<TEntity>() where TEntity : class
        {
            return _repositoryProvider.ProvideEntityRepository<TEntity>();
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}
