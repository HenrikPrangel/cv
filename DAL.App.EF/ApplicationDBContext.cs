﻿using DAL.Interfaces;
using Domain;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.App.EF
{
    public class ApplicationDBContext : IdentityDbContext<ApplicationUser>, IDataContext
    {
        public DbSet<Domain.Person> People { get; set; }
        public DbSet<Domain.Experience> Experiences { get; set; }
        public DbSet<Domain.Skills> Skills { get; set; }
        public DbSet<Domain.Language> Languages { get; set; }

        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options)
            : base(options)
        {
        }
    }
}
