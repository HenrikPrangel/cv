﻿using DAL.EF;
using DAL.Interfaces;
using DAL.Interfaces.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.App.EF.Helpers
{
    public class EFRepositoryFactoryProvider : IRepositoryFactoryProvider
    {

        public Func<IDataContext, object> GetFactoryForStandarRepo<TEntity>() where TEntity : class
        {
            return (context) => new EFRepository<TEntity>(context as ApplicationDBContext);
        }
    }
}
