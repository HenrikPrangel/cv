﻿using DAL.Interfaces;
using DAL.Interfaces.Helpers;
using DAL.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.App.EF.Helpers
{
    public class EFRepositoryProvider : IRepositoryProvider
    {
        private readonly Dictionary<Type, object> _repositoryCache = new Dictionary<Type, object>();
        private readonly ApplicationDBContext _context;
        private readonly IRepositoryFactoryProvider _factoryProvider;

        public EFRepositoryProvider(IDataContext context, IRepositoryFactoryProvider factoryProvider)
        {
            _factoryProvider = factoryProvider;
            _context = context as ApplicationDBContext;
        }

        public IRepository<TEntity> ProvideEntityRepository<TEntity>() where TEntity : class
        {
            return GetOrCreateRepository<IRepository<TEntity>>(
                _factoryProvider.GetFactoryForStandarRepo<TEntity>()
                );
        }

        TRepositoryInterface GetOrCreateRepository<TRepositoryInterface>(
            Func<ApplicationDBContext, object> repoCreationFunc) where TRepositoryInterface : class
        {
            _repositoryCache.TryGetValue(typeof(TRepositoryInterface), out var repo);
            if (repo != null)
            {
                return (TRepositoryInterface)repo;
            }

            if (repoCreationFunc == null)
            {
                throw new ArgumentNullException(nameof(repoCreationFunc));
            }

            repo = repoCreationFunc(_context);
            if (repo == null)
            {
                throw new NullReferenceException(typeof(TRepositoryInterface).FullName);
            }

            _repositoryCache.Add(typeof(TRepositoryInterface), repo);
            return (TRepositoryInterface)repo;
        }
    }
}
