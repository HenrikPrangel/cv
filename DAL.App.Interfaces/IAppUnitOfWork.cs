﻿using DAL.Interfaces;
using DAL.Interfaces.Repositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.App.Interfaces
{
    public interface IAppUnitOfWork : IUnitOfWork
    {
        IRepository<Person> People { get; }
        IRepository<Experience> Experiences { get; }
        IRepository<Skills> Skills { get; }
        IRepository<Language> Languages { get; }
    }
}
