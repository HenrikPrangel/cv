﻿using DAL.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.EF
{
    public class EFRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected DbContext RepositoryDbContext;
        protected DbSet<TEntity> RepositoryDbSet;

        public EFRepository(DbContext dbContext)
        {

            RepositoryDbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));

            RepositoryDbSet = RepositoryDbContext.Set<TEntity>();
        }

        public IEnumerable<TEntity> All(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = RepositoryDbSet.Count(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(TEntity).FullName} {count}/{maxAllowed}");
                }

            }

            var res = RepositoryDbSet.ToList();

            return res;
        }

        public virtual async Task<IEnumerable<TEntity>> AllAsync(int maxAllowed = 10)
        {
            if (!(maxAllowed <= 0 || maxAllowed == int.MaxValue))
            {
                var count = await RepositoryDbSet.CountAsync(); // select count(*) from dbset
                if (count > maxAllowed)
                {
                    throw new ApplicationException($"Too many rows in result! {typeof(TEntity).FullName} {count}/{maxAllowed}");
                }

            }
            return await RepositoryDbSet.ToListAsync();
        }
    }
}
