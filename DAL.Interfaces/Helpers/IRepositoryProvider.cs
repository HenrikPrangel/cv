﻿using DAL.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Interfaces.Helpers
{
    public interface IRepositoryProvider
    {
        IRepository<TEntity> ProvideEntityRepository<TEntity>()
    where TEntity : class;
    }
}
