﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        
        IEnumerable<TEntity> All(int maxAllowed = 10);

        Task<IEnumerable<TEntity>> AllAsync(int maxAllowed = 10);
    }
}
