﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Experience
    {
        public int ExperienceId { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime DateUntil { get; set; }

        [Required]
        [MaxLength(50)]
        public string InstitutionName { get; set; }

        [Required]
        [MaxLength(50)]
        public string InstitutionLocation { get; set; }

        [Required]
        [MaxLength(100)]
        public string Information { get; set; }

        public ExperienceType ExperienceType { get; set; }


        public int PersonId { get; set; }
        public Person Person { get; set; }

    }
}
