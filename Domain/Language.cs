﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Language
    {
        public int LanguageId { get; set; }

        [Required]
        [MaxLength(30)]
        public string LanguageName { get; set; }

        public int Understanding { get; set; }

        public int Speaking { get; set; }

        public int Writing { get; set; }

        [Required]
        [MaxLength(100)]
        public string Comment { get; set; }

        public int PersonId { get; set; }
        public Person Person { get; set; }
    }
}
