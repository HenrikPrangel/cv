﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Person
    {
        public int PersonId { get; set; }

        [Required]
        [MaxLength(60)]
        public string Firstname { get; set; }

        [Required]
        [MaxLength(60)]
        public string Lastname { get; set; }

        [Required]
        [MaxLength(100)]
        public string Address { get; set; }

        [Required]
        [MaxLength(60)]
        public string Email { get; set; }

        [Required]
        [MaxLength(50)]
        public string Phonenumber { get; set; }

        [Required]
        [MaxLength(250)]
        public string PersonDescription { get; set; }

        public List<Skills> PersonSkills { get; set; }

        public List<Language> PersonLanguages { get; set; }

        public List<Experience> PersonExperiences { get; set; }
    }
}
