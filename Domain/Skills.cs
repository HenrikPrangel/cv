﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Skills
    {
        public int SkillsId { get; set; }

        [Required]
        [MaxLength(40)]
        public string SkillName { get; set; }

        [Required]
        [MaxLength(40)]
        public string StudyTime { get; set; }

        [Required]
        [MaxLength(50)]
        public string Opinion { get; set; }


        public int PersonId { get; set; }
        public Person Person { get; set; }
    }
}
