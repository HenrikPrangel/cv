﻿using DAL.App.EF;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Helpers
{
    public class AppDataInitializator
    {
        public static void InitializeAppDatabase(ApplicationDBContext context)
        {
            if (!context.People.Any())
            {
                context.People.Add(new Domain.Person()
                {
                    Firstname = "Henrik",
                    Lastname = "Prangel",
                    Address = "Kännu 28a, Tallinn, 13418 ",
                    Email = "henrik.prangel@gmail.com",
                    Phonenumber = "(+372) 53441373",
                    PersonDescription = "At the moment, I'm a college student, studying IT, and hoping to put my knowledge to the test",
                });
                context.SaveChanges();
            }

            if (!context.Experiences.Any())
            {
                context.Experiences.Add(new Domain.Experience()
                {
                    PersonId = context.People.First().PersonId,
                    DateFrom = DateTime.ParseExact("20040101", "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None),
                    DateUntil = DateTime.ParseExact("20160101", "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None),
                    InstitutionName = "Rocca al Mare School",
                    InstitutionLocation = "Tallinn",
                    Information = "Secondary Education",
                    ExperienceType = 0


                });
                context.Experiences.Add(new Domain.Experience()
                {
                    PersonId = context.People.First().PersonId,
                    DateFrom = DateTime.ParseExact("20160101", "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None),
                    DateUntil = DateTime.ParseExact("20190101", "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None),
                    InstitutionName = "IT College",
                    InstitutionLocation = "Tallinn",
                    Information = "IT Systems Development",
                    ExperienceType = 0

                });
                context.SaveChanges();
            }

            if (!context.Languages.Any())
            {
                context.Languages.Add(new Domain.Language()
                {
                    PersonId = context.People.First().PersonId,
                    LanguageName = "Estonian",
                    Understanding = 5,
                    Speaking = 5,
                    Writing = 5,
                    Comment = "Native tongue"
                });
                context.Languages.Add(new Domain.Language()
                {
                    PersonId = context.People.First().PersonId,
                    LanguageName = "English",
                    Understanding = 5,
                    Speaking = 5,
                    Writing = 5,
                    Comment = "C2 level – given by CAE certificate"
                });
                context.Languages.Add(new Domain.Language()
                {
                    PersonId = context.People.First().PersonId,
                    LanguageName = "German",
                    Understanding = 3,
                    Speaking = 1,
                    Writing = 1,
                    Comment = "A2/B1 level, given by GLS Berlin certificate "
                });
                context.SaveChanges();
            }
            if (!context.Skills.Any())
            {
                context.Skills.Add(new Domain.Skills()
                {
                    PersonId = context.People.First().PersonId,
                    SkillName = "Java",
                    StudyTime = "Extensive",
                    Opinion = "Comfortable"
                });
                context.Skills.Add(new Domain.Skills()
                {
                    PersonId = context.People.First().PersonId,
                    SkillName = "C#",
                    StudyTime = "Moderate",
                    Opinion = "Comfortable"
                });
                context.Skills.Add(new Domain.Skills()
                {
                    PersonId = context.People.First().PersonId,
                    SkillName = "ASP.NET",
                    StudyTime = "Moderate",
                    Opinion = "Comfortable"
                });
                context.Skills.Add(new Domain.Skills()
                {
                    PersonId = context.People.First().PersonId,
                    SkillName = "MVC",
                    StudyTime = "Moderate",
                    Opinion = "Comfortable"
                });
                context.Skills.Add(new Domain.Skills()
                {
                    PersonId = context.People.First().PersonId,
                    SkillName = "JavaScript",
                    StudyTime = "Small",
                    Opinion = "Insecure"
                });
                context.Skills.Add(new Domain.Skills()
                {
                    PersonId = context.People.First().PersonId,
                    SkillName = "SQL",
                    StudyTime = "Small",
                    Opinion = "Insecure"
                });
                context.SaveChanges();
            }
        }
    }
}
