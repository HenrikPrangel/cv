﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.App.Interfaces;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace MyCVHenrikPrangel.Controllers
{
    public class ExperienceController : Controller
    {
        private readonly IAppUnitOfWork _uow;
        private readonly Person person;

        public ExperienceController(IAppUnitOfWork uow)
        {
            _uow = uow;
            person = _uow.People.All().First();

        }

        public IActionResult Education()
        {
            @ViewData["Page"] = "Education";
            @ViewData["FirstName"] = person.Firstname;
            @ViewData["LastName"] = person.Lastname;
            ViewData["Message"] = "Your application description page.";

            return View(_uow.Experiences.All().Where(x => x.ExperienceType == Domain.Enums.ExperienceType.Education && x.PersonId == person.PersonId).ToList());
        }

        public IActionResult WorkExperience()
        {
            @ViewData["Page"] = "Work Experience";
            @ViewData["FirstName"] = person.Firstname;
            @ViewData["LastName"] = person.Lastname;

            var exp = _uow.Experiences.All().Where(x => x.ExperienceType == Domain.Enums.ExperienceType.WorkExperience
            && x.PersonId == person.PersonId).ToList();
            if (exp.Count() < 1)
            {
                ViewData["Message"] = "At the moment, i have no professional work experience, besides the odd couple summer jobs, completely unrelated to IT";
            }
            return View(exp);
        }

    }
}