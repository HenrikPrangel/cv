﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.App.Interfaces;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace MyCVHenrikPrangel.Controllers
{
    public class PersonController : Controller
    {
        private readonly IAppUnitOfWork _uow;
        private readonly Person person;

        public PersonController(IAppUnitOfWork uow)
        {
            _uow = uow;
#warning This is fine for this project, but definetly needs to be reworked if made into a real website
            //Currently, only one person shall be held in the Database, so this works for now
            person = _uow.People.All().First();

        }


        public IActionResult Index()
        {
            @ViewData["FirstName"] = person.Firstname;
            @ViewData["LastName"] = person.Lastname;
            @ViewData["Email"] = person.Email;
            @ViewData["PersDesc"] = person.PersonDescription;
            @ViewData["PhoneNr"] = person.Phonenumber;
            @ViewData["Address"] = person.Address;
            return View();
        }
    }
}