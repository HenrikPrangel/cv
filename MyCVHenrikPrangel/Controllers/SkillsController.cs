﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.App.Interfaces;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace MyCVHenrikPrangel.Controllers
{
    public class SkillsController : Controller
    {
        private readonly IAppUnitOfWork _uow;
        private readonly Person person;

        public SkillsController(IAppUnitOfWork uow)
        {
            _uow = uow;
            person = _uow.People.All().First();

        }
        public IActionResult Skills()
        {
            @ViewData["Page"] = "Skills and Abilites";
            @ViewData["FirstName"] = person.Firstname;
            @ViewData["LastName"] = person.Lastname;
            ViewData["Message"] = "Your contact page.";

            return View(_uow.Skills.All().Where(x => x.PersonId == person.PersonId).ToList());
        }
        public IActionResult Languages()
        {
            @ViewData["Page"] = "Languages";
            @ViewData["FirstName"] = person.Firstname;
            @ViewData["LastName"] = person.Lastname;
            return View(_uow.Languages.All().Where(x => x.PersonId == person.PersonId).ToList());
        }
    }
}